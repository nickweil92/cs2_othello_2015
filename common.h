#ifndef __COMMON_H__
#define __COMMON_H__

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    double HScore;
    Move(int x, int y) {
        this->x = x;
        this->y = y;
        this->HScore = 0;     
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }

    void setHScore(double HScore) { this->HScore = HScore; }
    double getHScore() { return HScore; }
};

#endif
