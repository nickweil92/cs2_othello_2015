#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <list>
#include <cstdio>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
    Side my_side;
    Side opponentsSide;
    Board *the_board;
    std::vector<Move *> validMoves(Side side, Board* someBoard);
    int heuristic(Board* someBoard, Side side);
    int minimax_helper(Board *thisBoard, Side side, int depth, int alpha, int beta);
    Move *minimax(Board *currentBoard, Side side, int depth, int alpha, int beta);
    Side otherSide(Side side);
    
};

#endif
