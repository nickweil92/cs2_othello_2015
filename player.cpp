#include "player.h"

/*
 * Making a change for points!!! Change made by Nick Weil.
 */


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    // Initialize my side and opponent's side variables.
    this->my_side = side;

    if (side == WHITE)
    {
        this->opponentsSide = BLACK;
    }
    else
    {
        this->opponentsSide = WHITE;
    }
   
    // Initialize the board. 
    the_board = new Board();

    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 



    // Process the opponent's move.
    if (opponentsMove != NULL)
    {
        the_board->doMove(opponentsMove, opponentsSide);
    }

    // Calculate my own move.

    // Get list of my own moves
    std::list<Move*>* validMoveList = validMoves(my_side, the_board);
    if (validMoveList->empty())
    {
        return NULL;
    }

    Move *bestMove = (*(validMoveList->begin()));

    for (std::list<Move*>::iterator i = validMoveList->begin(); i != validMoveList->end(); i++) 
    {
        int HScore = heuristic(the_board, my_side);
       /* (*i)->setHScore(HScore);
        if (HScore > bestMove->getHScore())
        {
            bestMove = (*i);
        }*/
    }
   // the_board->doMove(bestMove, my_side);

    return bestMove;
}


std::vector<Move*>* Player::validMoves(Side side, Board* someBoard) {
    std::vector<Move*>* validMoves = new std::vector<Move*>();
    Move* tempMove = new Move(0,0); 
    for (int i = 0; i < 8; i++) {
        tempMove->setX(i);
        for (int j = 0; j < 8; j++) {
            tempMove->setY(j);
            if (someBoard->checkMove(tempMove, side)) {
                Move* ToBePushedMove = new Move(i,j);
                validMoves->push_back(ToBePushedMove);
            }
        }
    }
    delete tempMove;
    return validMoves;
}


int Player::heuristic(Board* someBoard, Side side) {
    int counter = 0;
    std::vector<int> valMap {10000, -3000, 1000, 800, 800, 1000, -3000, 10000, -3000, -5000, -450, -500, -500, -450, -5000, -3000, 1000,-450, 30, 10, 10, 30, -450, 1000, 800, -500, 10, 50, 50, 10, -500, 800, 800, -500, 10, 50 , 50, 10, -500, 800, 1000, -450, 30, 10, 10, 30, -450, 1000, -3000, -5000, -450, -500, -500, -450, -5000, -3000, 10000, -3000, 1000, 800, 300, 1000, -3000, 10000};
    for (int i = 0; i < 8)
    {
        for (int j = 0; j < 8)
        {
            if (someBoard->get(side, i, j)
            {
                count += valMap[i+8*j];
            }
            else if (someboard->get(otherSide(side), i,j)
            {
                count -= valMap[i+8*j];
            }
        }
    }
/*   
    if ((x == 0 || x == 7) && (y == 0 || y == 7))
    {
        tempHVal += 5;
    }
    else if (((x == 1 || x ==6) && (y == 0 || y == 7)) || ((y == 1 || y ==6) && (x == 0 || x == 7)))
    {
        tempHVal -= 5;
    }
    else if ((x == 0 || x == 7) || (y == 0 || y == 7))
    {
        tempHVal += 3;
    }
*/
    counter += count(side) - count(otherSide(side));
    return counter;
}


int Player::minimax_helper(Board *thisBoard, Side side, int depth, int alpha, int beta) {
    if (depth == 0) {return heuristic(board, side);}
    std::vector<Move*>* validMoveList = validMoves(side, thisBoard);
    if (validMoveList.size() == 0) 
    { 
        if (thisBoard->hasMoves(otherSide(side)))
        {
            return heuristic(thisBoard, side);
        }
        int val = -minimax_helper(thisBoard, otherSide(side), depth-1,-beta,-alpha);
        if (val >= beta) 
        {
            delete validMoveList;
            return val;
        }
        if (val > alpha) {alpha = val;}
    }
    else
    {
        for(std::vector<int>::iterator it =(*validMoveList).begin(); it != validMoveList.end(); it++)
        {
            Board *nextBoard = currentBoard->copy();
            nextBoard->doMove((*it), side);
            int val = -minimax_helper(nextBoard, otherSide(side), depth-1,-beta,-alpha);
            if (val >= beta) 
            {
                delete validMoveList;
                return val;
            }
            if (val > alpha) { alpha = val; }
        }          
    }
    return alpha;
}



Move *Player::minimax(Board *currentBoard, Side side, int depth, int alpha, int beta)
{
    int alpha = -70;
    int beta = 70;
    
    if (depth == 0)
    {
        return currentBoard->count(side);
    }
    std::vector<Move*>* validMoveList = validMoves(side, currentBoard);
    Move *bestMove = validMoveList[0];
    for (auto it = validMoveList.begin(); it != validMoveList.end(); it++)
    {
        Board *nextBoard = currentBoard->copy();
        nextBoard->doMove(it, side);
        int val = minimax_helper(nextBoard, otherSide(side), depth-1,-beta,-alpha);
        if (val >= beta) { return it; }
        if (val > alpha) {
            alpha = val;
            bestMove = it;
        }
    }
    return bestMove;

}







Side Player::otherSide(Side side) {
    if (side == BLACK) { return WHITE; }
    else {return BLACK; }
}















